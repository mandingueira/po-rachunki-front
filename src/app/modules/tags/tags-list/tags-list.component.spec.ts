import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {TagsListComponent} from './tags-list.component';
import {Tag} from '../../../shared/models/interfaces/tag.interface';
import {mockedTags} from '../../../../../tests/mockedData';
import {of} from 'rxjs';
import {TagsService} from '../../../core/services/tags.service';

describe('TagsListComponent', () => {
  let component: TagsListComponent;
  let fixture: ComponentFixture<TagsListComponent>;
  let tagsService: TagsService;
  const tags: Tag[] = mockedTags;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TagsListComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [TagsService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagsListComponent);
    tagsService = TestBed.get(TagsService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create tags', () => {
    expect(component).toBeDefined();
  });

  it('should filter PurchaseType tags', () => {
    expect(component.tagsPurchaseType.length).toEqual(0);
    component.filterTags(tags);
    expect(component.tagsPurchaseType.length).toEqual(4);
  });

  it('should filter product tags', () => {
    expect(component.tagsProduct.length).toEqual(0);
    component.filterTags(tags);
    expect(component.tagsProduct[0].length + component.tagsProduct[1].length + component.tagsProduct[2].length
      + component.tagsProduct[3].length).toEqual(4);
  });

  it('should filter brand tags', () => {
    expect(component.tagsBrand.length).toEqual(0);
    component.filterTags(tags);
    expect(component.tagsBrand[0].length + component.tagsBrand[1].length + component.tagsBrand[2].length +
      component.tagsBrand[3].length).toEqual(1);
  });

  it('should filter shop tags', () => {
    expect(component.tagsShop.length).toEqual(0);
    component.filterTags(tags);
    expect(component.tagsPurchaseType.length).toEqual(4);
    expect(component.tagsShop[0].length + component.tagsShop[1].length + component.tagsShop[2].length +
      component.tagsShop[3].length).toEqual(2);
  });

  it('should call getTags and call filterTags with response', async(() => {
    const response: Tag[] = mockedTags;
    fixture.detectChanges();
    spyOn(tagsService, 'getTags').and.returnValue(of(response));
    fixture.detectChanges();
    spyOn(component, 'filterTags');
    component.getTags();
    expect(component.filterTags).toHaveBeenCalledWith(response);
  }));


  it('should call addTags and call getTags with response', async(() => {
    const response: Tag = mockedTags[6];
    const tag: Tag = mockedTags[0];
    fixture.detectChanges();
    spyOn(tagsService, 'addTag').and.returnValue(of(response));
    fixture.detectChanges();
    spyOn(component, 'getTags');
    component.addTag(tag);
    expect(component.getTags).toHaveBeenCalled();
  }));
});

