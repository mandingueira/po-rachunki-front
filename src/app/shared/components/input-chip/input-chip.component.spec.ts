import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {InputChipComponent} from './input-chip.component';
import {MatChipsModule, MatFormFieldModule, MatIconModule} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {By} from '@angular/platform-browser';
import {DebugElement, Input} from '@angular/core';
import {mockedTags} from '../../../../../tests/mockedData';
import {Tag} from '../../models/interfaces/tag.interface';

describe('InputChipComponent', () => {
  let component: InputChipComponent;
  let fixture: ComponentFixture<InputChipComponent>;
  let el: DebugElement;
  let removeButton: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputChipComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatIconModule,
        MatChipsModule,
        MatFormFieldModule,
        BrowserAnimationsModule,
      ],
      providers: [InputChipComponent]
    });
  }));

  beforeEach(() => {
    component = TestBed.get(InputChipComponent);
    fixture = TestBed.createComponent(InputChipComponent);
    el = fixture.debugElement.query(By.css('no-category-type'));
    removeButton = fixture.debugElement.query(By.css('removable-tag'));
    fixture.detectChanges();
  });

  it('should create chips', () => {
    expect(component).toBeDefined();
  });

  it('should emit', () => {
    spyOn(component.getSelectedTag, 'emit');
    component.selectTag(mockedTags[0]);
    fixture.detectChanges();
    expect(component.getSelectedTag.emit).toHaveBeenCalledWith(mockedTags[0]);
  });

  // it('should create chips with category', () => {
  //   component.catType = 'product';
  //   component.removable = true;
  //   fixture.detectChanges();
  //   expect(document.getElementsByClassName('no-category-type')).toBeDefined();
  // });

  // it('should create chips with category', () => {
  //   // component.catType = mockedTags;
  //   component.category = 'Product';
  //   component.tagsByType = mockedTags;
  //   component.tagType = 'moda';
  //   component.removable = true;
  //   fixture.detectChanges();
  //   removeButton.triggerEventHandler('click', mockedTags[5]);
  //   fixture.detectChanges();
  //   expect(component.removeSelectedTag).toHaveBeenCalledWith(mockedTags[5]._id);
  // });

});

